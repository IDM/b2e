# B2E  

## Authors

P. Elleaume, J. Chavanne, ESRF

## Introduction

The B2E software is dedicated to compute synchrotron radiation from arbitrary magnetic field. 

An interface to Igor Pro 8 is provided.

## Contents

This repository contains:

- *source:* C++ sources and header files
- *vc:* Visual Studio 2017 project files
- *xcode:* Xcode files (TO DO)
- *xop:*
  - *res:* resources files for Igor XOPs
  - *xop8*: binaries for Igor XOP8 extension

## Dependencies

The Igor XOP Toolkit is needed for building this project. See Wavemetric's website: https://www.wavemetrics.com/products/xoptoolkit.

The following structure of the repository is assumed:

- *external_libs*
  - *XOPToolkit*
    - *XOPToolkit8*
      - *IgorXOPs8*
        - *XOPSupport*
- *b2e (this project)*

## Installation

1. Create a shortcut to *B2E/igor_proc*, rename it as *b2e_proc* and move it to *Documents\WaveMetrics\Igor Pro 8 User Files\Igor Procedures*
2. Navigate to the folder corresponding to the XOP version you need, *e.g.* *B2E/xop/xop8*
3. Create a shortcut to this folder, rename it as *b2e* and move it to *Documents\WaveMetrics\Igor Pro 8 User Files\Igor Extensions* or *Igor Extensions (64-bit)* depending on the platform

