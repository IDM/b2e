#pragma once

/*
	B2E.h -- equates for B2E XOP
*/

// Custom error codes
#define OLD_IGOR 1 + FIRST_XOP_ERR

/* Prototypes */
HOST_IMPORT int XOPMain(IORecHandle ioRecHandle);