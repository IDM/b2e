#include <math.h>
//#include <stdlib.h>


#include "Lienard.h"

#define NMAX  1000


/*________________________________________________________________________*/

int Lienard(elec el, angle an, flux flu, double ds, double gtx, double gtz, double cst_)

/*	Sources :
		Radiation from an Insertion Device with arbitrary field
		by P. Elleaume, ESRF-SR/ID-89-31
*/


{
	register long j;
	register double cst, phi, cc = 0, ss = 0;
	/*
	float *pux, *puz, *pphi;
	*/
	double pux[NMAX], puz[NMAX], pphi[NMAX];
	long i, jmax, snpt;
	double sdep, sstep, sfin;
	double phen, cst1, cst2, cst3, cst4;
	double rax, iax, raz, iaz, s;
	long jb, jc; double js;
	double alp, bet, gamma;

	snpt = an.npt; sdep = an.dep; sstep = an.step;
	sfin = sdep + (snpt - 1) * sstep;


	jmax = (sfin - sdep) / ds;

	if (jmax > NMAX) return 1;

	/*
	pux = malloc( sizeof(float)*jmax );
	puz = malloc( sizeof(float)*jmax );
	pphi = malloc( sizeof(float)*jmax );
	*/

	cst1 = 0.5 * 586.7 * ds;
	cst2 = 7.735e18 * el.current / el.energy / el.energy * ds * ds;
	cst4 = 661.52 / el.energy / el.energy * ds;
	gamma = 1956.95 * el.energy;



	j = 0;

	for (s = sdep + ds; s <= sfin - ds; s += ds)
	{
		js = (s - sdep) / sstep;


		jb = (long)js; jc = jb + 1;
		alp = jc - js; bet = js - jb;

		pux[j] = gamma * (alp * an.px[jb] + bet * an.px[jc]) - gtx;
		puz[j] = gamma * (alp * an.pz[jb] + bet * an.pz[jc]) - gtz;

		pphi[j] = 1 + (pux[j] * pux[j] + puz[j] * puz[j]);

		j++;
	}

	jmax = j - 1;

	for (i = 0; i < flu.npt; i++)
	{
		phen = flu.dep + i * flu.step;
		cst = phen * cst4; cst3 = cst2 * phen * phen * cst_;
		rax = 0; raz = 0; iax = 0; iaz = 0; phi = 0;

		for (j = 0; j <= jmax; j++)
		{

			phi += cst * pphi[j];
			cc = cos((double)phi); ss = sin((double)phi);

			rax += pux[j] * cc; raz += puz[j] * cc;
			iax += pux[j] * ss; iaz += puz[j] * ss;

		}

		flu.pt[i] += (float)cst3 * (rax * rax + raz * raz + iax * iax + iaz * iaz);
		flu.px[i] += (float)cst3 * (rax * rax + iax * iax);
		flu.pi[i] += (float)cst3 * ((rax + raz) * (rax + raz) + (iax + iaz) * (iax + iaz)) * 0.5;
		flu.pc[i] += (float)cst3 * ((rax - iaz) * (rax - iaz) + (raz + iax) * (raz + iax)) * 0.5;

	}
	/*
	free(pux); free(puz); free(pphi);
	*/
	return 0;
}
/*________________________________________________________________________*/

int Lienardnew_(elec el, angle an, flux flu, double ds, double gtx, double gtz, double cst_, double* pux, double* puz, double* pphi)

/*	Sources :
		Radiation from an Insertion Device with arbitrary field
		by P. Elleaume, ESRF-SR/ID-89-31
*/


{
	register long j;
	register double cst, phi, cc = 0, ss = 0;


	long i, jmax, snpt;
	double sdep, sstep, sfin;
	double phen, cst1, cst2, cst3, cst4;
	double rax, iax, raz, iaz, s;
	long jb, jc; double js;
	double alp, bet, gamma;

	snpt = an.npt; sdep = an.dep; sstep = an.step;
	sfin = sdep + (snpt - 1) * sstep;


	jmax = (sfin - sdep) / ds;



	cst1 = 0.5 * 586.7 * ds;
	cst2 = 7.735e18 * el.current / el.energy / el.energy * ds * ds;
	cst4 = 661.52 / el.energy / el.energy * ds;
	gamma = 1956.95 * el.energy;



	j = 0;

	for (s = sdep + ds; s <= sfin - ds; s += ds)
	{
		js = (s - sdep) / sstep;


		jb = (long)js; jc = jb + 1;
		alp = jc - js; bet = js - jb;

		pux[j] = gamma * (alp * an.px[jb] + bet * an.px[jc]) - gtx;
		puz[j] = gamma * (alp * an.pz[jb] + bet * an.pz[jc]) - gtz;

		pphi[j] = 1 + (pux[j] * pux[j] + puz[j] * puz[j]);

		j++;
	}

	jmax = j - 1;

	for (i = 0; i < flu.npt; i++)
	{
		phen = flu.dep + i * flu.step;
		cst = phen * cst4; cst3 = cst2 * phen * phen * cst_;
		rax = 0; raz = 0; iax = 0; iaz = 0; phi = 0;

		for (j = 0; j <= jmax; j++)
		{

			phi += cst * pphi[j];
			cc = cos((double)phi); ss = sin((double)phi);

			rax += pux[j] * cc; raz += puz[j] * cc;
			iax += pux[j] * ss; iaz += puz[j] * ss;

		}

		flu.pt[i] += (double)cst3 * (rax * rax + raz * raz + iax * iax + iaz * iaz);
		flu.px[i] += (double)cst3 * (rax * rax + iax * iax);
		flu.pi[i] += (double)cst3 * ((rax + raz) * (rax + raz) + (iax + iaz) * (iax + iaz)) * 0.5;
		flu.pc[i] += (double)cst3 * ((rax - iaz) * (rax - iaz) + (raz + iax) * (raz + iax)) * 0.5;

	}
	return 0;
}
/*________________________________________________________________________*/

int AngLienard(elec el, angle an, double phen, fluxa flu, double ds, double cst_, double* pux, double* puz, double* pphi)

{
	long j;
	double cst, phi, cc = 0, ss = 0;


	long i, jmax, snpt;
	double sdep, sstep, sfin;
	double cst1, cst2, cst3, cst4;
	double gtx, gtz;
	double rax, iax, raz, iaz, s;
	long jb, jc; double js;
	double alp, bet, gamma;

	snpt = an.npt; sdep = an.dep; sstep = an.step;
	sfin = sdep + (snpt - 1) * sstep;


	jmax = (sfin - sdep) / ds;



	cst1 = 0.5 * 586.7 * ds;
	cst2 = 7.735e18 * el.current / el.energy / el.energy * ds * ds;
	cst4 = 661.52 / el.energy / el.energy * ds;
	gamma = 1956.95 * el.energy;



	for (i = 0; i < flu.npt; i++)
	{
		cst = phen * cst4; cst3 = cst2 * phen * phen * cst_;
		rax = 0; raz = 0; iax = 0; iaz = 0; phi = 0;
		gtx = flu.gtxd + (flu.gtxf - flu.gtxd) * i / (flu.npt - 1);
		gtz = flu.gtzd + (flu.gtzf - flu.gtzd) * i / (flu.npt - 1);

		j = 0;
		for (s = sdep + ds; s <= sfin - ds; s += ds)
		{
			js = (s - sdep) / sstep;


			jb = (long)js; jc = jb + 1;
			alp = jc - js; bet = js - jb;

			pux[j] = gamma * (alp * an.px[jb] + bet * an.px[jc]) - gtx;
			puz[j] = gamma * (alp * an.pz[jb] + bet * an.pz[jc]) - gtz;
			pphi[j] = 1 + (pux[j] * pux[j] + puz[j] * puz[j]);

			phi += cst * pphi[j];
			cc = cos((double)phi); ss = sin((double)phi);

			rax += pux[j] * cc; raz += puz[j] * cc;
			iax += pux[j] * ss; iaz += puz[j] * ss;
			j++;
		}

		flu.pt[i] += (float)cst3 * (rax * rax + raz * raz + iax * iax + iaz * iaz);
		flu.px[i] += (float)cst3 * (rax * rax + iax * iax);
		flu.pi[i] += (float)cst3 * ((rax + raz) * (rax + raz) + (iax + iaz) * (iax + iaz)) * 0.5;
		flu.pc[i] += (float)cst3 * ((rax - iaz) * (rax - iaz) + (raz + iax) * (raz + iax)) * 0.5;

	}
	return 0;
}

/*________________________________________________________________________*/
void interp(angle* aa, double* s, double* bx, double* bz)
{
	static double imed, alp, bet; static long imin, imax;
	imed = (*s - aa->dep) / aa->step;
	imin = imed;
	if (imin < aa->npt - 1) {
		imax = imin + 1; alp = imax - imed; bet = imed - imin;
		*bx = aa->px[imin] * alp + aa->px[imax] * bet;
		*bz = aa->pz[imin] * alp + aa->pz[imax] * bet;
	}
	else {
		*bx = aa->px[aa->npt - 1];
		*bz = aa->pz[aa->npt - 1];
	}

}
/*________________________________________________________________________*/

int Champ(elec el, angle an, double gtx, double gtz, angle field)
/* computes the electric field starting from the angle */
{
	double gamma, s, cs, ds2, bx, bz, tx, tz, k1, k2; long i;
	double px, pz, px1, px2, pz1, pz2;
	gamma = 1956.95 * el.energy;

	px2 = 0; pz2 = 0;

	s = an.dep;
	cs = 2 * gamma * gamma * field.step;
	ds2 = 1 / (field.step * el.energy);

	/* runge Kutta or order 2 */

	interp(&an, &s, &bx, &bz);
	tx = gamma * bx - gtx; tz = gamma * bz - gtz;
	k1 = cs / (1 + tx * tx + tz * tz);
	px1 = tx * k1;
	pz1 = tz * k1;

	field.px[0] = 0.; field.pz[0] = 0.;

	for (i = 1; i < field.npt; i++)
	{
		s += k1;
		interp(&an, &s, &bx, &bz);
		tx = gamma * bx - gtx;
		tz = gamma * bz - gtz;
		k2 = cs / (1 + tx * tx + tz * tz);
		s += (k2 - k1) / 2;
		interp(&an, &s, &bx, &bz);
		tx = gamma * bx - gtx; tz = gamma * bz - gtz;
		k1 = cs / (1 + tx * tx + tz * tz);
		px = tx * k1;
		pz = tz * k1;
		if (i > 1)
		{
			field.px[i - 1] = (px - px2) / 2;
			field.pz[i - 1] = (pz - pz2) / 2;
		}
		else
		{
			field.px[0] = (px - px1);
			field.pz[0] = (pz - pz1);
		}
		px2 = px1; px1 = px; pz2 = pz1; pz1 = pz;
	}
	field.px[i - 1] = (px1 - px2);
	field.pz[i - 1] = (pz1 - pz2);

	for (i = 0; i < field.npt; i++) { field.px[i] *= ds2; field.pz[i] *= ds2; }
	return 0;
}
/*________________________________________________________________________*/

double length(elec el, angle an, double gtx, double gtz, double step)
{
	double gamma, s, cs, bx, bz, tx, tz, k1, k2; long i;

	gamma = 1956.95 * el.energy;

	s = an.dep;
	cs = 2 * gamma * gamma * step;

	/* runge Kutta or order 2 */

	interp(&an, &s, &bx, &bz);
	tx = gamma * bx - gtx; tz = gamma * bz - gtz; k1 = cs / (1 + tx * tx + tz * tz);

	for (i = 1;; i++)
	{

		s += k1;
		interp(&an, &s, &bx, &bz);
		tx = gamma * bx - gtx; tz = gamma * bz - gtz; k2 = cs / (1 + tx * tx + tz * tz);
		s += (k2 - k1) / 2;
		interp(&an, &s, &bx, &bz);
		tx = gamma * bx - gtx; tz = gamma * bz - gtz; k1 = cs / (1 + tx * tx + tz * tz);
		if (s > an.dep + an.step * an.npt) return i * step;
	}
}
