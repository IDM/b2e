#pragma once

typedef struct {
	double emitt; /* [m] */
	double beta; /* [m] */
	double alpha; /* [m] */
	double eta; /* [m] */
	double etap; /* [m] */
	double av; /* [mm] */
	double avp; /* [mm] */
} plan;

typedef struct {
	double energy; /* [GeV] */
	double current;/* [Amp] */
	double sige;/*[] */
	plan p[2];/* 1: x,   2: z */
} elec;

typedef struct {
	long npt;
	double dep, step;
	double* px, * pz;
} angle;

typedef struct {
	long npt;
	double dep, step;
	double* pt, * px, * pi, * pc;
} flux;


typedef struct {
	long npt;
	double gtxd, gtzd, gtxf, gtzf;
	double* pt, * px, * pi, * pc;
} fluxa;

void interp(angle*, double*, double*, double*);
int Lienard(elec el, angle an, flux flu, double ds, double gtx, double gtz, double cst);
int Lienardnew_(elec el, angle an, flux flu, double ds, double gtx, double gtz, double cst, double* pux, double* puz, double* pphi);
int AngLienard(elec el, angle an, double phen, fluxa flu, double ds, double cst_, double* pux, double* puz, double* pphi);
int Champ(elec el, angle an, double gtx, double gtz, angle field);
double length(elec el, angle an, double gtx, double gtz, double step);

