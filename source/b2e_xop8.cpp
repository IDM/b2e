/*	B2E.c -- A sample Igor external operation

    B2E is a very simple Igor XOP (external operation) designed to show you how to get
    started writing XOPs.

    The syntax for B2E is:
        B2E wave

    It adds 1 to the specified wave.
    It generates an error message if the wave is complex or text (not numeric).

    HR, 091021
        Updated for 64-bit compatibility.

    HR, 2013-02-08
        Updated for Xcode 4 compatibility. Changed to use XOPMain instead of main.
        As a result the XOP now requires Igor Pro 6.20 or later.

    HR, 2018-05-04
        Recompiled with XOP Toolkit 8 which supports long object names.
        As a result the XOP now requires Igor Pro 8.00 or later.
*/

#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h

#include "b2e_xop8.h"
#include "Lienard.h"

// Global Variables (none)
// Operation template: champ_ wave:Ax, wave:Az, wave:Ex, Wave:Ez, wave:params

#pragma pack(2)        // All structures passed to Igor are two-byte aligned.
struct DPComplexNum {
    double real;
    double imag;
};
#pragma pack()        // Reset structure alignment to default.


//---------------------------------------------------------------------------------
static int
champ_(waveHndl Ax, waveHndl Az, waveHndl Ex, waveHndl Ez, waveHndl params)
{
    elec el;
    angle an;
    angle field;
    double gtx;
    double gtz;
    double* pa;

    switch (WaveType(Ax))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Az))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Ex))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Ez))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(params))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    pa = (double*)WaveData(params);
    an.px = (double*)WaveData(Ax);
    an.pz = (double*)WaveData(Az);
    field.px = (double*)WaveData(Ex);
    field.pz = (double*)WaveData(Ez);
    an.npt = pa[5];
    an.dep = pa[6];
    an.step = pa[7];
    el.energy = pa[0];
    //   el.current=pa[1];
    gtx = pa[2];
    gtz = pa[3];
    field.npt = pa[8];
    field.dep = pa[9];
    field.step = pa[10];
    Champ(el, an, gtx, gtz, field);
    return 0;
}
/*________________________________________________________________________*/
static int
Length(waveHndl Ax, waveHndl Az, waveHndl params)
{
    elec el;
    angle an;
    double gtx;
    double gtz;
    double* pa;
    double step;

    switch (WaveType(Ax))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Az))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(params))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    pa = (double*)WaveData(params);
    an.px = (double*)WaveData(Ax);
    an.pz = (double*)WaveData(Az);
    an.npt = pa[5];
    an.dep = pa[6];
    an.step = pa[7];
    el.energy = pa[0];
    //   el.current=pa[1];
    gtx = pa[2];
    gtz = pa[3];
    step = pa[10];
    pa[4] = length(el, an, gtx, gtz, step);
    return 0;
}
/*________________________________________________________________________*/
static int Stokes_inter(waveHndl Wz, waveHndl Wx, waveHndl Wst, waveHndl Wsx, waveHndl Wsi, waveHndl Wsr, waveHndl Wpa)

{
    int res = 0;
    DPComplexNum* pz, * px;
    double* pst, * psx, * psi, * psr, * ppa;
    long i, jr, ji;
    double cst, cst2, rax, raz, iax, iaz;

    switch (WaveType(Wz))
    {
    case (NT_FP64 | NT_CMPLX):
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wx))
    {
    case (NT_FP64 | NT_CMPLX):
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wst))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsi))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsr))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpa))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    pz = (DPComplexNum*)WaveData(Wz);
    px = (DPComplexNum*)WaveData(Wx);
    pst = (double*)WaveData(Wst);
    psx = (double*)WaveData(Wsx);
    psi = (double*)WaveData(Wsi);
    psr = (double*)WaveData(Wsr);
    ppa = (double*)WaveData(Wpa);


    cst = ppa[0]; cst2 = cst / 2;

    for (i = 0; i < WavePoints(Wst); i++) {

        rax = px[i].real; iax = px[i].imag;
        raz = pz[i].real; iaz = pz[i].imag;
        pst[i] += cst * (rax * rax + raz * raz + iax * iax + iaz * iaz);
        psx[i] += cst * (rax * rax + iax * iax);
        psi[i] += cst2 * ((rax + raz) * (rax + raz) + (iax + iaz) * (iax + iaz));
        psr[i] += cst2 * ((rax - iaz) * (rax - iaz) + (raz + iax) * (raz + iax));

    }
    return(res);
}
/*________________________________________________________________________*/
static int Lienardnew_inter(waveHndl Wz, waveHndl Wx, waveHndl Wst, waveHndl Wsx, waveHndl Wsi, waveHndl Wsr, waveHndl Wpa, waveHndl Wpx, waveHndl Wpz, waveHndl Wph)

{
    int res = 0;
    double* pa, * px, * pz, * ph;
    double ds, gtx, gtz, cst;
    elec el;
    angle an;
    flux flu;

    switch (WaveType(Wz))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wst))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsi))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsr))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpa))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpz))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wph))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }

    an.px = (double*)WaveData(Wx);
    an.pz = (double*)WaveData(Wz);
    flu.pt = (double*)WaveData(Wst);
    flu.px = (double*)WaveData(Wsx);
    flu.pi = (double*)WaveData(Wsi);
    flu.pc = (double*)WaveData(Wsr);
    pa = (double*)WaveData(Wpa);
    px = (double*)WaveData(Wpx);
    pz = (double*)WaveData(Wpz);
    ph = (double*)WaveData(Wph);

    an.npt = pa[5]; an.dep = pa[6]; an.step = pa[7];
    flu.npt = pa[8]; flu.dep = pa[9]; flu.step = pa[10];

    el.energy = pa[0];
    el.current = pa[1];
    gtx = pa[2]; gtz = pa[3];
    ds = pa[4];
    cst = pa[11];


    res = Lienardnew_(el, an, flu, ds, gtx, gtz, cst, px, pz, ph);

    return(res);
}
/*________________________________________________________________________*/
static int AngLienard_inter(waveHndl Wz, waveHndl Wx, waveHndl Wst, waveHndl Wsx, waveHndl Wsi, waveHndl Wsr, waveHndl Wpa, waveHndl Wpx, waveHndl Wpz, waveHndl Wph)

{
    int res = 0;
    double* pa, * px, * pz, * ph;
    double ds, cst, phen;
    elec el;
    angle an;
    fluxa flu;

    switch (WaveType(Wz))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wst))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsi))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wsr))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpa))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpx))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wpz))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }
    switch (WaveType(Wph))
    {
    case NT_FP64:
        break;
    case TEXT_WAVE_TYPE:                        // Don't handle text waves.
        return NO_TEXT_OP;
    default:                                    // Don't handle complex or integer for now.
        return NT_FNOT_AVAIL;
    }

    an.px = (double*)WaveData(Wx);
    an.pz = (double*)WaveData(Wz);
    flu.pt = (double*)WaveData(Wst);
    flu.px = (double*)WaveData(Wsx);
    flu.pi = (double*)WaveData(Wsi);
    flu.pc = (double*)WaveData(Wsr);
    pa = (double*)WaveData(Wpa);
    px = (double*)WaveData(Wpx);
    pz = (double*)WaveData(Wpz);
    ph = (double*)WaveData(Wph);

    el.energy = pa[0];
    el.current = pa[1];
    phen = pa[2];
    flu.gtxd = pa[3];
    flu.gtzd = pa[4];
    flu.gtxf = pa[5];
    flu.gtzf = pa[6];
    flu.npt = pa[7];
    an.npt = pa[8];
    an.dep = pa[9];
    an.step = pa[10];
    ds = pa[11];
    cst = pa[12];

    res = AngLienard(el, an, phen, flu, ds, cst, px, pz, ph);

    return(res);
}
/*________________________________________________________________________*/

// Operation template: lienarnew wave:Bxa, wave:Bza, wave:Ft, Wave:Fx, wave:Fi, wave:Fr, wave:param, wave:px, wave:pz, wave:phi

// Runtime param structure for lienarnew operation.
#pragma pack(2)    // All structures passed to Igor are two-byte aligned.
struct lienardnewRuntimeParams {
    // Flag parameters.

    // Main parameters.

    // Parameters for simple main group #0.
    int BxaEncountered;
    waveHndl Bxa;
    int BxaParamsSet[1];

    // Parameters for simple main group #1.
    int BzaEncountered;
    waveHndl Bza;
    int BzaParamsSet[1];

    // Parameters for simple main group #2.
    int FtEncountered;
    waveHndl Ft;
    int FtParamsSet[1];

    // Parameters for simple main group #3.
    int FxEncountered;
    waveHndl Fx;
    int FxParamsSet[1];

    // Parameters for simple main group #4.
    int FiEncountered;
    waveHndl Fi;
    int FiParamsSet[1];

    // Parameters for simple main group #5.
    int FrEncountered;
    waveHndl Fr;
    int FrParamsSet[1];

    // Parameters for simple main group #6.
    int paramEncountered;
    waveHndl param;
    int paramParamsSet[1];

    // Parameters for simple main group #7.
    int pxEncountered;
    waveHndl px;
    int pxParamsSet[1];

    // Parameters for simple main group #8.
    int pzEncountered;
    waveHndl pz;
    int pzParamsSet[1];

    // Parameters for simple main group #9.
    int phiEncountered;
    waveHndl phi;
    int phiParamsSet[1];

    // These are postamble fields that Igor sets.
    int calledFromFunction;                    // 1 if called from a user function, 0 otherwise.
    int calledFromMacro;                    // 1 if called from a macro, 0 otherwise.
};
typedef struct lienardnewRuntimeParams lienardnewRuntimeParams;
typedef struct lienardnewRuntimeParams* lienardnewRuntimeParamsPtr;
#pragma pack()    // Reset structure alignment to default.
/*________________________________________________________________________*/

// Operation template: anglienarnew wave:Bxa, wave:Bza, wave:Ft, Wave:Fx, wave:Fi, wave:Fr, wave:param, wave:px, wave:pz, wave:phi

// Runtime param structure for lienarnew operation.
#pragma pack(2)    // All structures passed to Igor are two-byte aligned.
struct AngLienardRuntimeParams {
    // Flag parameters.

    // Main parameters.

    // Parameters for simple main group #0.
    int BxaEncountered;
    waveHndl Bxa;
    int BxaParamsSet[1];

    // Parameters for simple main group #1.
    int BzaEncountered;
    waveHndl Bza;
    int BzaParamsSet[1];

    // Parameters for simple main group #2.
    int FtEncountered;
    waveHndl Ft;
    int FtParamsSet[1];

    // Parameters for simple main group #3.
    int FxEncountered;
    waveHndl Fx;
    int FxParamsSet[1];

    // Parameters for simple main group #4.
    int FiEncountered;
    waveHndl Fi;
    int FiParamsSet[1];

    // Parameters for simple main group #5.
    int FrEncountered;
    waveHndl Fr;
    int FrParamsSet[1];

    // Parameters for simple main group #6.
    int paramEncountered;
    waveHndl param;
    int paramParamsSet[1];

    // Parameters for simple main group #7.
    int pxEncountered;
    waveHndl px;
    int pxParamsSet[1];

    // Parameters for simple main group #8.
    int pzEncountered;
    waveHndl pz;
    int pzParamsSet[1];

    // Parameters for simple main group #9.
    int phiEncountered;
    waveHndl phi;
    int phiParamsSet[1];

    // These are postamble fields that Igor sets.
    int calledFromFunction;                    // 1 if called from a user function, 0 otherwise.
    int calledFromMacro;                    // 1 if called from a macro, 0 otherwise.
};
typedef struct AngLienardRuntimeParams AngLienardRuntimeParams;
typedef struct AngLienardRuntimeParams* AngLienardRuntimeParamsPtr;
#pragma pack()    // Reset structure alignment to default.

/*________________________________________________________________________*/
#pragma pack(2)    // All structures passed to Igor are two-byte aligned.
struct champ_RuntimeParams {
    // Flag parameters.

    // Main parameters.

    // Parameters for simple main group #0.
    int AxEncountered;
    waveHndl Ax;
    int AxParamsSet[1];

    // Parameters for simple main group #1.
    int AzEncountered;
    waveHndl Az;
    int AzParamsSet[1];

    // Parameters for simple main group #2.
    int ExEncountered;
    waveHndl Ex;
    int ExParamsSet[1];

    // Parameters for simple main group #3.
    int EzEncountered;
    waveHndl Ez;
    int EzParamsSet[1];

    // Parameters for simple main group #4.
    int paramsEncountered;
    waveHndl params;
    int paramsParamsSet[1];

    // These are postamble fields that Igor sets.
    int calledFromFunction;                    // 1 if called from a user function, 0 otherwise.
    int calledFromMacro;                    // 1 if called from a macro, 0 otherwise.
};
typedef struct champ_RuntimeParams champ_RuntimeParams;
typedef struct champ_RuntimeParams* champ_RuntimeParamsPtr;
#pragma pack()    // Reset structure alignment to default.
/*________________________________________________________________________*/
// Operation template: Lengt_ wave:Ax, wave:Az, wave:params

// Runtime param structure for Lengt_ operation.
#pragma pack(2)    // All structures passed to Igor are two-byte aligned.
struct Lengt_RuntimeParams {
    // Flag parameters.

    // Main parameters.

    // Parameters for simple main group #0.
    int AxEncountered;
    waveHndl Ax;
    int AxParamsSet[1];

    // Parameters for simple main group #1.
    int AzEncountered;
    waveHndl Az;
    int AzParamsSet[1];

    // Parameters for simple main group #2.
    int paramsEncountered;
    waveHndl params;
    int paramsParamsSet[1];

    // These are postamble fields that Igor sets.
    int calledFromFunction;                    // 1 if called from a user function, 0 otherwise.
    int calledFromMacro;                    // 1 if called from a macro, 0 otherwise.
};
typedef struct Lengt_RuntimeParams Lengt_RuntimeParams;
typedef struct Lengt_RuntimeParams* Lengt_RuntimeParamsPtr;
#pragma pack()    // Reset structure alignment to default.
/*________________________________________________________________________*/

// Operation template: Stokes wave:a2, wave:a1, wave:St, Wave:Sx, Wave:Si, Wave:Sr, wave:param

// Runtime param structure for Stokes operation.
#pragma pack(2)    // All structures passed to Igor are two-byte aligned.
struct StokesRuntimeParams {
    // Flag parameters.

    // Main parameters.

    // Parameters for simple main group #0.
    int a2Encountered;
    waveHndl a2;
    int a2ParamsSet[1];

    // Parameters for simple main group #1.
    int a1Encountered;
    waveHndl a1;
    int a1ParamsSet[1];

    // Parameters for simple main group #2.
    int StEncountered;
    waveHndl St;
    int StParamsSet[1];

    // Parameters for simple main group #3.
    int SxEncountered;
    waveHndl Sx;
    int SxParamsSet[1];

    // Parameters for simple main group #4.
    int SiEncountered;
    waveHndl Si;
    int SiParamsSet[1];

    // Parameters for simple main group #5.
    int SrEncountered;
    waveHndl Sr;
    int SrParamsSet[1];

    // Parameters for simple main group #6.
    int paramEncountered;
    waveHndl param;
    int paramParamsSet[1];

    // These are postamble fields that Igor sets.
    int calledFromFunction;                    // 1 if called from a user function, 0 otherwise.
    int calledFromMacro;                    // 1 if called from a macro, 0 otherwise.
};
typedef struct StokesRuntimeParams StokesRuntimeParams;
typedef struct StokesRuntimeParams* StokesRuntimeParamsPtr;
#pragma pack()    // Reset structure alignment to default.

/*________________________________________________________________________*/
extern "C" int
Executechamp_(champ_RuntimeParamsPtr p)
{

    waveHndl Ax;
    waveHndl Az;
    waveHndl Ex;
    waveHndl Ez;
    waveHndl params;
    elec el;
    angle an;
    double gtx;
    double gtz;
    angle field;

    int err = 0;

    // Main parameters.

    if (p->AxEncountered) {
        if (p->AxParamsSet[0] == 0)
            return NOWAV;
        Ax = p->Ax;
        if (Ax == NULL)
            return NOWAV;
        // Parameter: p->Ax (test for NULL handle before using)
    }

    if (p->AzEncountered) {
        if (p->AzParamsSet[0] == 0)
            return NOWAV;
        Az = p->Az;
        if (Az == NULL)
            return NOWAV;
        // Parameter: p->Az (test for NULL handle before using)
    }

    if (p->ExEncountered) {
        if (p->ExParamsSet[0] == 0)
            return NOWAV;
        Ex = p->Ex;
        if (Ex == NULL)
            return NOWAV;
        // Parameter: p->Ex (test for NULL handle before using)
    }

    if (p->EzEncountered) {
        if (p->EzParamsSet[0] == 0)
            return NOWAV;
        Ez = p->Ez;
        if (Ez == NULL)
            return NOWAV;
        // Parameter: p->Ez (test for NULL handle before using)
    }

    if (p->paramsEncountered) {
        if (p->paramsParamsSet[0] == 0)
            return NOWAV;
        params = p->params;
        if (params == NULL)
            return NOWAV;

        // Parameter: p->params (test for NULL handle before using)
    }
    err = champ_(Ax, Az, Ex, Ez, params);
    if (err == 0)
    {
        WaveHandleModified(Ex);
        WaveHandleModified(Ez);
    }
    return err;
}
//**************************************************************************************
extern "C" int
ExecuteLengt_(Lengt_RuntimeParamsPtr p)
{
    waveHndl Ax;
    waveHndl Az;
    waveHndl params;

    int err = 0;

    // Main parameters.

    if (p->AxEncountered)
    {
        if (p->AxParamsSet[0] == 0)
            return NOWAV;
        Ax = p->Ax;
        if (Ax == NULL)
            return NOWAV;
        // Parameter: p->Ax (test for NULL handle before using)
    }

    if (p->AzEncountered)
    {
        if (p->AzParamsSet[0] == 0)
            return NOWAV;
        Az = p->Az;
        if (Az == NULL)
            return NOWAV;
        // Parameter: p->Az (test for NULL handle before using)
    }

    if (p->paramsEncountered)
    {
        if (p->paramsParamsSet[0] == 0)
            return NOWAV;
        params = p->params;
        if (params == NULL)
            return NOWAV;
    }
    // Parameter: p->params (test for NULL handle before using)
    err = Length(Ax, Az, params);
    if (err == 0)
    {
        WaveHandleModified(params);
    }

    return err;
}
//******************************************************************************************
extern "C" int
ExecuteStokes(StokesRuntimeParamsPtr p)
{
    waveHndl Wz, Wx, Wst, Wsx, Wsi, Wsr, Wpa;
    int err = 0;

    // Main parameters.

    if (p->a2Encountered)
    {
        if (p->a2ParamsSet[0] == 0)
            return NOWAV;
        Wz = p->a2;
        if (Wz == NULL)
            return NOWAV;
        // Parameter: p->a2 (test for NULL handle before using)
    }
    if (p->a1Encountered)
    {
        if (p->a1ParamsSet[0] == 0)
            return NOWAV;
        Wx = p->a1;
        if (Wx == NULL)
            return NOWAV;
        // Parameter: p->a1 (test for NULL handle before using)
    }
    if (p->StEncountered)
    {
        if (p->StParamsSet[0] == 0)
            return NOWAV;
        Wst = p->St;
        if (Wst == NULL)
            return NOWAV;
        // Parameter: p->St (test for NULL handle before using)
    }

    if (p->SxEncountered)
    {
        if (p->SxParamsSet[0] == 0)
            return NOWAV;
        Wsx = p->Sx;
        if (Wsx == NULL)
            return NOWAV;
        // Parameter: p->Sx (test for NULL handle before using)
    }
    if (p->SiEncountered)
    {
        if (p->SiParamsSet[0] == 0)
            return NOWAV;
        Wsi = p->Si;
        if (Wsi == NULL)
            return NOWAV;
        // Parameter: p->Si (test for NULL handle before using)
    }

    if (p->SrEncountered)
    {
        if (p->SrParamsSet[0] == 0)
            return NOWAV;
        Wsr = p->Sr;
        if (Wsr == NULL)
            return NOWAV;
        // Parameter: p->Sr (test for NULL handle before using)
    }

    if (p->paramEncountered) {
        if (p->paramParamsSet[0] == 0)
            return NOWAV;
        Wpa = p->param;
        if (Wpa == NULL)
            return NOWAV;
        // Parameter: p->params (test for NULL handle before using)
    }
    err = Stokes_inter(Wz, Wx, Wst, Wsx, Wsi, Wsr, Wpa);
    if (err == 0)
    {
        WaveHandleModified(Wst);
        WaveHandleModified(Wsx);
        WaveHandleModified(Wsi);
        WaveHandleModified(Wsr);
    }
    return err;
}
//******************************************************************************************

extern "C" int
Executelienardnew(lienardnewRuntimeParamsPtr p)
{
    waveHndl Bxa, Bza, Ft, Fx, Fi, Fr, param, px, pz, phi;
    int err = 0;

    // Main parameters.

    if (p->BxaEncountered)
    {
        if (p->BxaParamsSet[0] == 0)
            return NOWAV;
        Bxa = p->Bxa;
        if (Bxa == NULL)
            return NOWAV;
        // Parameter: p->Bxa (test for NULL handle before using)
    }

    if (p->BzaEncountered)
    {
        if (p->BzaParamsSet[0] == 0)
            return NOWAV;
        Bza = p->Bza;
        if (Bza == NULL)
            return NOWAV;
        // Parameter: p->Bza (test for NULL handle before using)
    }

    if (p->FtEncountered)
    {
        if (p->FtParamsSet[0] == 0)
            return NOWAV;
        Ft = p->Ft;
        if (Ft == NULL)
            return NOWAV;
        // Parameter: p->Ft (test for NULL handle before using)
    }

    if (p->FxEncountered)
    {
        if (p->FxParamsSet[0] == 0)
            return NOWAV;
        Fx = p->Fx;
        if (Fx == NULL)
            return NOWAV;
        // Parameter: p->Fx (test for NULL handle before using)
    }

    if (p->FiEncountered)
    {
        if (p->FiParamsSet[0] == 0)
            return NOWAV;
        Fi = p->Fi;
        if (Fi == NULL)
            return NOWAV;
        // Parameter: p->Fi (test for NULL handle before using)
    }

    if (p->FrEncountered)
    {
        if (p->FrParamsSet[0] == 0)
            return NOWAV;
        Fr = p->Fr;
        if (Fr == NULL)
            return NOWAV;
        // Parameter: p->Fr (test for NULL handle before using)
    }

    if (p->paramEncountered)
    {
        if (p->paramParamsSet[0] == 0)
            return NOWAV;
        param = p->param;
        if (param == NULL)
            return NOWAV;
        // Parameter: p->param (test for NULL handle before using)
    }

    if (p->pxEncountered)
    {
        if (p->pxParamsSet[0] == 0)
            return NOWAV;
        px = p->px;
        if (px == NULL)
            return NOWAV;
        // Parameter: p->px (test for NULL handle before using)
    }

    if (p->pzEncountered)
    {
        if (p->pzParamsSet[0] == 0)
            return NOWAV;
        pz = p->pz;
        if (pz == NULL)
            return NOWAV;
        // Parameter: p->pz (test for NULL handle before using)
    }

    if (p->phiEncountered)
    {
        if (p->phiParamsSet[0] == 0)
            return NOWAV;
        phi = p->phi;
        if (phi == NULL)
            return NOWAV;
        // Parameter: p->phi (test for NULL handle before using)
    }
    err = Lienardnew_inter(Bxa, Bza, Ft, Fx, Fi, Fr, param, px, pz, phi);
    if (err == 0)
    {
        WaveHandleModified(Ft);
        WaveHandleModified(Fx);
        WaveHandleModified(Fi);
        WaveHandleModified(Fr);
    }

    return err;
}
//******************************************************************************************

extern "C" int
ExecuteAngLienard(AngLienardRuntimeParamsPtr p)
{
    waveHndl Bxa, Bza, Ft, Fx, Fi, Fr, param, px, pz, phi;
    int err = 0;

    // Main parameters.

    if (p->BxaEncountered)
    {
        if (p->BxaParamsSet[0] == 0)
            return NOWAV;
        Bxa = p->Bxa;
        if (Bxa == NULL)
            return NOWAV;
        // Parameter: p->Bxa (test for NULL handle before using)
    }

    if (p->BzaEncountered)
    {
        if (p->BzaParamsSet[0] == 0)
            return NOWAV;
        Bza = p->Bza;
        if (Bza == NULL)
            return NOWAV;
        // Parameter: p->Bza (test for NULL handle before using)
    }

    if (p->FtEncountered)
    {
        if (p->FtParamsSet[0] == 0)
            return NOWAV;
        Ft = p->Ft;
        if (Ft == NULL)
            return NOWAV;
        // Parameter: p->Ft (test for NULL handle before using)
    }

    if (p->FxEncountered)
    {
        if (p->FxParamsSet[0] == 0)
            return NOWAV;
        Fx = p->Fx;
        if (Fx == NULL)
            return NOWAV;
        // Parameter: p->Fx (test for NULL handle before using)
    }

    if (p->FiEncountered)
    {
        if (p->FiParamsSet[0] == 0)
            return NOWAV;
        Fi = p->Fi;
        if (Fi == NULL)
            return NOWAV;
        // Parameter: p->Fi (test for NULL handle before using)
    }

    if (p->FrEncountered)
    {
        if (p->FrParamsSet[0] == 0)
            return NOWAV;
        Fr = p->Fr;
        if (Fr == NULL)
            return NOWAV;
        // Parameter: p->Fr (test for NULL handle before using)
    }

    if (p->paramEncountered)
    {
        if (p->paramParamsSet[0] == 0)
            return NOWAV;
        param = p->param;
        if (param == NULL)
            return NOWAV;
        // Parameter: p->param (test for NULL handle before using)
    }

    if (p->pxEncountered)
    {
        if (p->pxParamsSet[0] == 0)
            return NOWAV;
        px = p->px;
        if (px == NULL)
            return NOWAV;
        // Parameter: p->px (test for NULL handle before using)
    }

    if (p->pzEncountered)
    {
        if (p->pzParamsSet[0] == 0)
            return NOWAV;
        pz = p->pz;
        if (pz == NULL)
            return NOWAV;
        // Parameter: p->pz (test for NULL handle before using)
    }

    if (p->phiEncountered)
    {
        if (p->phiParamsSet[0] == 0)
            return NOWAV;
        phi = p->phi;
        if (phi == NULL)
            return NOWAV;
        // Parameter: p->phi (test for NULL handle before using)
    }
    err = AngLienard_inter(Bxa, Bza, Ft, Fx, Fi, Fr, param, px, pz, phi);
    if (err == 0)
    {
        WaveHandleModified(Ft);
        WaveHandleModified(Fx);
        WaveHandleModified(Fi);
        WaveHandleModified(Fr);
    }

    return err;
}

//******************************************************************************************
static int
Registerchamp_(void)
{
    const char* cmdTemplate;
    const char* runtimeNumVarList;
    const char* runtimeStrVarList;

    // NOTE: If you change this template, you must change the champ_RuntimeParams structure as well.
    cmdTemplate = "champ_ wave:Ax, wave:Az, wave:Ex, Wave:Ez, wave:params";
    runtimeNumVarList = "";
    runtimeStrVarList = "";
    return RegisterOperation(cmdTemplate, runtimeNumVarList, runtimeStrVarList, sizeof(champ_RuntimeParams), (void*)Executechamp_, 0);
}
/*________________________________________________________________________*/
static int
RegisterLengt_(void)
{
    const char* cmdTemplate;
    const char* runtimeNumVarList;
    const char* runtimeStrVarList;

    // NOTE: If you change this template, you must change the Lengt_RuntimeParams structure as well.
    cmdTemplate = "Lengt_ wave:Ax, wave:Az, wave:params";
    runtimeNumVarList = "";
    runtimeStrVarList = "";
    return RegisterOperation(cmdTemplate, runtimeNumVarList, runtimeStrVarList, sizeof(Lengt_RuntimeParams), (void*)ExecuteLengt_, 0);
}
/*________________________________________________________________________*/
static int
RegisterStokes(void)
{
    const char* cmdTemplate;
    const char* runtimeNumVarList;
    const char* runtimeStrVarList;

    // NOTE: If you change this template, you must change the StokesRuntimeParams structure as well.
    cmdTemplate = "Stokes wave:a2, wave:a1, wave:St, Wave:Sx, Wave:Si, Wave:Sr, wave:param";
    runtimeNumVarList = "";
    runtimeStrVarList = "";
    return RegisterOperation(cmdTemplate, runtimeNumVarList, runtimeStrVarList, sizeof(StokesRuntimeParams), (void*)ExecuteStokes, 0);
}
/*________________________________________________________________________*/
static int
Registerlienarnew(void)
{
    const char* cmdTemplate;
    const char* runtimeNumVarList;
    const char* runtimeStrVarList;

    // NOTE: If you change this template, you must change the lienarnewRuntimeParams structure as well.
    cmdTemplate = "lienardnew wave:Bxa, wave:Bza, wave:Ft, Wave:Fx, wave:Fi, wave:Fr, wave:param, wave:px, wave:pz, wave:phi";
    runtimeNumVarList = "";
    runtimeStrVarList = "";
    return RegisterOperation(cmdTemplate, runtimeNumVarList, runtimeStrVarList, sizeof(lienardnewRuntimeParams), (void*)Executelienardnew, 0);
}
/*________________________________________________________________________*/
static int
RegisterAngLienard(void)
{
    const char* cmdTemplate;
    const char* runtimeNumVarList;
    const char* runtimeStrVarList;

    // NOTE: If you change this template, you must change the lienarnewRuntimeParams structure as well.
    cmdTemplate = "AngLienard wave:Bxa, wave:Bza, wave:Ft, Wave:Fx, wave:Fi, wave:Fr, wave:param, wave:px, wave:pz, wave:phi";
    runtimeNumVarList = "";
    runtimeStrVarList = "";
    return RegisterOperation(cmdTemplate, runtimeNumVarList, runtimeStrVarList, sizeof(AngLienardRuntimeParams), (void*)ExecuteAngLienard, 0);
}

/*________________________________________________________________________*/

static int
RegisterOperations(void)        // Register any operations with Igor.
{
    int result;


    if (result = Registerchamp_())
        return result;
    if (result = RegisterLengt_())
        return result;
    if (result = RegisterStokes())
        return result;
    if (result = Registerlienarnew())
        return result;
    if (result = RegisterAngLienard())
        return result;

    // There are no more operations added by this XOP.

    return 0;
}
/*________________________________________________________________________*/

extern "C" void
XOPEntry(void)
{
    XOPIORecResult result = 0;

    // We don't need to handle any messages for this XOP.
    // switch (GetXOPMessage()) {
    // }

    SetXOPResult(result);
}

/*	XOPMain(ioRecHandle)

    This is the initial entry point at which the host application calls XOP.
    The message sent by the host must be INIT.

    XOPMain does any necessary initialization and then sets the XOPEntry field of the
    ioRecHandle to the address to be called for future messages.
*/
/*________________________________________________________________________*/

HOST_IMPORT int
XOPMain(IORecHandle ioRecHandle)
{
    int result;

    XOPInit(ioRecHandle);				// Do standard XOP initialization.
    SetXOPEntry(XOPEntry);				// Set entry point for future calls.

    if (igorVersion < 800) {			// XOP Toolkit 8.00 or later requires Igor Pro 8.00 or later
        SetXOPResult(OLD_IGOR);			// OLD_IGOR is defined in B2E.h and there are corresponding error strings in B2E.r and B2EWinCustom.rc.
        return EXIT_FAILURE;
    }

    if (result = RegisterOperations()) {
        SetXOPResult(result);
        return EXIT_FAILURE;
    }

    SetXOPResult(0);
    return EXIT_SUCCESS;
}
